import { RestaurantService } from './restaurants.service';
import { Restaurant } from './restaurant/restaurant.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mt-restaurants',
  templateUrl: './restaurants.component.html',
  styleUrls: ['./restaurants.component.css']
})
export class RestaurantsComponent implements OnInit {

  restaurants: Restaurant[]

  constructor(private restaurantService: RestaurantService) { }

  ngOnInit() {
    this.restaurantService.resturants().subscribe(restaurants => this.restaurants = restaurants);
  } //sempre vai ser executado quando o componente for chamado
  //.subscribe vai receber o mapeamento do objeto restaurants e vai atribuir a variavel restaurants

}
