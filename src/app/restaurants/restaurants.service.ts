import { MEAT_API } from './../app.api';
import { Http } from '@angular/http';
import { Restaurant } from './restaurant/restaurant.model';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';


@Injectable()
export class RestaurantService {

   

    constructor(private http: Http) {

    }

    resturants(): Observable<Restaurant[]> {
        return this.http.get(`${MEAT_API}/restaurants`).map(response => response.json());

    } // metodo chamado resturants, onde ele ai retornar um array de restaurant
 


}