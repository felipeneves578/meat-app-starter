import { Restaurant } from './restaurant.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'mt-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.css']
})
export class RestaurantComponent implements OnInit {

  @Input() restaurant: Restaurant // permite que outros componentes passem informações para o componente resturant
  // a variavel restaurant vai receber uma interface do tipo Restaurant, onde contem as variaveis necessarias do resturante
  constructor() { }

  ngOnInit() {
  }

}
