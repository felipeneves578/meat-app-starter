import { RestaurantsComponent } from './restaurants/restaurants.component';

import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { Component } from '@angular/core';
import {Routes} from '@angular/router';


export const ROUTES: Routes = [
    {path: '', component: HomeComponent},
    {path: 'about', component: AboutComponent},
    {path: 'restaurants', component: RestaurantsComponent}
    //adicionar uma nova rota
    
]